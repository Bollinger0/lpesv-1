<?PHP

include('classes/EmailController.php');

$email_repository = new EmailRepository();
$email_repository->set_env('local');

$email_controller = new EmailController();

$email_controller->set_data_source($email_repository);

$email_controller->set_email_from('from address');
$email_controller->set_email_body('html body of email');
$email_controller->set_email_reply_to('where user will reply to');

$email_controller->send_email_process();
