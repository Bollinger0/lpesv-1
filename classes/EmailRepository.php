<?PHP

class EmailRepository
{
    public $env;

    public function __construct()
    {
        $this->create_methods_table();
        $this->create_emails_table();
    }

    public function set_env($env)
    {
    	if ($env == 'local')
	    {
	    	// connect to local server
		    $this->dbh = new PDO('mysql:host=localhost;dbname=dbname', 'user', 'pass');
	    }
	    else
	    {
		    // connect to live server
		    $this->dbh = new PDO('mysql:host=localhost;dbname=livedb', 'liveuser', 'livepass');
	    }
    }

    public function save_email()
    {
    	$confirmation_token = rand();
        $stmt = $this->dbh->prepare(
            "INSERT INTO emails(email) VALUES(:email)"
        );
        $stmt->execute(array('email' => $email));
        $this->save_confirmation_token($confirmation_token);
    }

    public function save_confirmation_token()
    {
		// Save token to database $this->
    }

    public function is_confirmation_token_valid()
    {
    	if (isset($_GET['confirmation_token']))
	    {
		    // check with database and return true/false depending of if it exists or not
	    }
    }

    public function set_email_as_confirmed()
    {
    	// Set validity field to 1 in database, we don't yet have that field tho
    }

	public function create_methods_table()
	{
		$create_methods_able_query = "
          CREATE TABLE IF NOT EXISTS methods(
            id int not null primary key auto_increment,
            method varchar(200)
            )";
		$this->dbh->exec($create_methods_able_query);
	}

	public function create_emails_table()
	{
		$create_emails_table_query = '
          CREATE TABLE IF NOT EXISTS emails(
          id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
          email varchar(100),
          sent boolean DEFAULT null)';

		$this->dbh->exec($create_emails_table_query);
	}

}
