<?PHP

class EmailController
{
    public $email;
    protected $data_source;
	private $email_from;
	private $email_body;
	public $message_to_user = array();
	private $email_reply_to;


    public function __construct()
    {
        $this->createEmailsTable();
        $this->createMethodsTable();
        $this->set_email();
    }

    public function set_data_source($data_source)
    {
		$this->data_source = $data_source;
    }


    public function set_email_from($email_from)
    {
		$this->email_from = $email_from;
    }

    public function set_email_body($email_body)
    {
		$this->email_body = $email_body;
    }

    public function set_email_reply_to($email_reply_to)
    {
	    $this->email_reply_to = $email_reply_to;
    }

    private function set_email()
    {
	    if (isset($_POST['submit']))
	    {
		    $this->email = $_POST['email'];
	    }
    }

	public function validate_email()
	{
        if (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
	}


	public function confirm_token_process()
	{
		if (isset($_GET['confirmation_token']))
		{
			if ($this->data_source->is_confirmation_token_valid())
			{
				$this->data_source->set_email_as_confirmed($this->email);
			}
			// if token is valid then remove token from database.
			// this means user has clicked the link we sent them via email
			// and email is valid.
		}
	}

    public function send_email_process()
    {
    	$this->save_email_process();

        if ($this->validate_email())
        {
            if (mail($this->email, 'TrafficGangsterClub - Free Methods', $this->email_body))
            {
            	$this->message_to_user[] = 'Download links have been sent to your email. If you can\'t find it check your spam inbox or try another email';
            }
            else
            {
                $this->message_to_user[] = 'Email failed to be sent';
            }
        }
        else
        {
	        $this->message_to_user[] = 'Invalid email';
        }
    }

	public function save_email_process()
	{
		if ($this->validate_email())
		{
			$this->data_source->save_email($this->email);
            $this->message_to_user[] = 'Will send the methods as soon as i get time';
        }
        else
		{
			$this->message_to_user = 'Invalid email';
        }
	}

	public function get_methods()
	{
        $selectMethodsQuery = "SELECT * FROM methods ORDER BY id DESC";
        return $this->dbh->query($selectMethodsQuery);
    }

}
